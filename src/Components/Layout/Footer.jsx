import React from "react";
import "../scss/footer.css";

function FooterBoard() {
  return (
    <div className="footer">
      <div className="h" style={{ textAlign: "center" }}>
        WHY MAZZZAYLO ?
      </div>
      <div className="footer-btns">
        <div className="btns footer-btns">Turn-key Crypto-mining Consultancy</div>
        <div className="btns footer-btns">100% Transparency</div>
        <div className="btns footer-btns">Strong Ethics</div>
        <div className="btns footer-btns">Professional Managenment</div>
        <div className="btns footer-btns">Lower Risk</div>
        <div className="btns footer-btns">Maximum Profits</div>
        <div className="btns footer-btns">Responsive Customer Support</div>
      </div>
    </div>
  );
}

export default FooterBoard;
