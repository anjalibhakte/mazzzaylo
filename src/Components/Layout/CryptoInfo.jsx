import React from "react";
import "../scss/middle.css";

function CryptoInfo() {
  return (
    <div>
      <div
        style={{
          color: "white",
          backgroundColor: "rgb(32, 32, 32)",
          borderRadius: 20,
          textAlign: "justify",
          padding: 30,
        }}
      >
        <p>
          A business entity deeply entrenched on the bedrock of ethical business
          practices, we at Mazzzaylo have a vast knowledge pool of crypto
          mining, complememnted with technical prowess that makes us one the
          most reliable names in crypto mining and crypto minining consultancy
        </p>
        <p>
          With our steadfast commitment to accountability, allegiance to lower
          risks and continual client support, we aspire to make cryptocurrency
          mining and exchange easier to understand, undertake and profit from.
        </p>
        <h3 className="">With us, your future can be yours today.</h3>
      </div>
    </div>
  );
}

export default CryptoInfo;
