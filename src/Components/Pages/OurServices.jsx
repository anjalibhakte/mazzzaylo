import React from "react";
import { Button } from "reactstrap";
import "../scss/pages.css";
import ShowMoreText from "react-show-more-text";

export default function OurServices() {
  return (
    <div style={{ backgroundColor: "black", color: "white" }}>
      <div>
        <h className="h">OUR SERVICES</h>
      </div>
      <div>
        <h1 className="h1">1) CRYPTOMINING</h1>
        <h2 className="h2">MINE YOUR WAY TO SUCCESS With Crypto Mining</h2>
        <ShowMoreText
          className="w-full"
          lines={3}
          more={<span>Show more</span>}
          less={<span>Show less</span>}
          expanded={false}
          truncatedEndingComponent={"... "}
          width={1500}
        >
          <p className="p">
            A process of using high-tech hardware to verify the legitimacy of
            crypto transactions and enter new crypto-coins into circulation,
            crypto mining is the fastest growing business avenues across the
            globe.
          </p>
          <p className="p">
            Anyone with a sophisticated computer can become a crypto miner. The
            miner is basically a third-party impartial verification through
            computer system that enables exchange of crypto-coins and prevents
            double spending of a particular coin.
          </p>
          <p className="p">
            A crypto-coin can be sent to a person only after being verified by a
            neutral computer that runs a specific software. This verification is
            called proof of work which decides whether the transaction can take
            place or get rejected.
          </p>
          <p className="p">
            Once you become a miner, you receive a fee for each verified
            transaction and is recorded in the blockchain, just like banks that
            earn from your transactions. They are rewards paid to you for
            decentralising the process and helping the blockchain function while
            reducing any chances of network attacks.
          </p>
          <p className="p">
            Therefore, the main question you should ask yourself is if you
            believe in the project and does it hold good future prospect. Only
            if the answer is Yes, does it make sense to get into crypto mining.
          </p>
          <p className="p">
            And all this is done with information-sharing at every step along
            the way, so you know exactly where you are every day.
          </p>
        </ShowMoreText>
      </div>
      <p>With us, mining will no longer be an ‘underground’ activity.</p>
      <h2 className="h2">TYPES OF MINING HARDWARE</h2>
      <h3 className="h3">• Graphics Card</h3>
      <p className="p">
        GPU mining involves the use of a gaming computer's graphics processing
        unit to solve complex math problems to verify electronic transactions on
        a blockchain. Graphics cards are equipped with a large number of
        Arithmetic Logic Units (ALU), which are responsible for performing
        mathematical computations. These enable the GPU to perform more
        calculations, leading to improved output for the crypto mining process.
      </p>
      <h3 className="h3">• ASIC (Application Specific Integrated Circuits)</h3>
      <p className="p">
        An application-specific integrated circuit (ASIC) is an integrated
        circuit chip that has been designed for a specific purpose, in this case
        bitcoin mining. Usually, an ASIC miner is built to mine a specific
        digital currency. So, a Bitcoin ASIC miner can mine only bitcoin.
        Developing and constructing ASICs mining devices is costly and complex
        but as they are built especially for mining cryptocurrency, they are
        faster than less powerful computers.
      </p>
      <h3 className="h3">• Motherboard/ Hard drive (less preferred)</h3>
      <p className="p">
        In this a motherboard or a hard drive is used to mine cryptocurrency.
        Unlike traditional mining based on graphics processing units (also known
        as GPU mining), hard disk miners use hard disk storage to generate new
        data blocks in the distributed ledger and receive rewards.
      </p>
      <h1 className="h1" style={{ textAlign: "left" }}>
        2) CRYPTOMINING CONSUTANCY
      </h1>
      <h2 className="h2">
        REAP THE REWARDS OF Our impeccable Crypto Mining Consultancy
      </h2>
      <p className="p">
        So, if you are planning to set up a crypto mining company, our team of
        expert professionals would be glad to guide you at every step, right
        from acquiring the necessary hardware, software, to the dos and don’ts
        of crypto transactions and expanding your crypto horizons, handholding
        you at every step through our rich experience in past that helps us
        offer you future insights of the crypto universe. Trust us to help you
        step into this exciting new world with assurance, certainty and
        credibility. ithe khute add karaych aahe
      </p>
      <h2 className="h2">HOW THIS WORKS</h2>
      <div className="p">
        Always wanted to start your own crypto-mining operation but don’t know
        how to? You’ve come to the right place!
      </div>
      <p className="p">
        Whether you are beginner or an experienced investor, we provide
        profitable crypto mining consultancy to everyone. Our technical
        expertise and industry experience give us an unmatched edge. All you
        need to do is provide us some details to kickstart your lucrative mining
        operations.
      </p>
      <h3 className="h3">THE PROCESS</h3>
      <p className="p">
        Our crypto mining consultancy is designed to provide you with end-to-end
        assistance in starting your own crypto-mining operation. Right from
        procuring to assembling to running to mining payouts we take care of
        everything.
      </p>
      <h3 className="h3">ANALYZE</h3>
      <p className="p">
        We run different simulations after understanding your budget, risk
        tolerance, and the project viability.
      </p>
      <h3 className="h3">DESIGN</h3>
      <p className="p">
        Post analysis we suggest reliable hardware as per your need.
      </p>
      <h3 className="h3">BUILD</h3>
      <p className="p">
        <>
          1) As soon as the design is ready, we procure hardware, assemble them
          into a high- performance mining rig and ship it to your location.
        </>
        <>
          2) We provide a list of necessary hardware and a guide on how to
          install the same.
        </>
        <>
          3) Once the set-up is complete you can start mining with us (ASIC or
          GPU)
        </>
      </p>
      <h3 className="h3">MANAGE</h3>
      <p className="p">
        Our constant support ensures enhanced efficiency and easy maintenance of
        your crypto mining operations.
      </p>
      <h1 className="h1">3) BLOCKCHAIN WALLET</h1>
      <p className="p">
        A blockchain wallet is a digital wallet that allows users to store,
        manage, and trade your cryptocurrencies and digital assets safely and
        efficiently. An e-wallet is created for free after you provide your
        email address and set a password. After account verification, you will
        be provided with a Wallet ID – a unique identification number similar to
        your bank account number. You can use your wallet anytime through our
        website.
      </p>
    </div>
  );
}
