import React from "react";
import "../scss/middle.css";

function Opportunities() {
  return (
    <div className="mid1">
    <div className="d-flex">
      <div className="w-25">
        <img  className="img" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQea1QzCx4hRz4Fw98N5Ijgb-yrklQGhOKniA&usqp=CAU"></img></div>
      <div className="w-75">
        <h className="h">
          A MINE OF GLOBAL OPPORTUNITIES
        </h>
        <p style={{ color: "white", marginTop:'25px', textAlign:'justify' }}>
          Crypto isn't the future, it's the present. As you read this, there are
          millions of people across the globe making transctions on different
          blackchains, a chain on which cryptocurrencies transactions are
          ledgered and stored worldwide.
        </p>
        <p style={{ color: "white" }}>
          Welcome to Mazzaylo, a reliable name in crypto minining and crypto
          mining consultancy that believes in transparent transactions, ethical
          business practices and futuristic vision. We help you tread
          confidently into the world of this versatile investment instrument and
          enable you to earn profits while creating diverse assests.
        </p>
      </div>
    </div>
    </div>
  );
}

export default Opportunities;
