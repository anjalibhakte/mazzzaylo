import React from "react";
import "../scss/pages.css";
function AboutUs() {
  return (
    <div style={{backgroundColor:'black', color:'white', height:'100vh'}}>
      <h className="h">ABOUT US</h>
      <p>
        A business entity deeply entrenched on the bedrock of ethical business
        practices, we at Mazzzaylo have a vast knowledge pool of crypto mining,
        complemented with technical prowess that makes us one the most reliable
        names in crypto mining and crypto mining consultancy.
      </p>
      <p>
        With our steadfast commitment to accountability, allegiance to lower
        risks and continual client support, we aspire to make cryptocurrency
        mining and exchange easier to understand, undertake and profit from.
      </p>
      <div className="line">With us, your future can be yours today.</div>
    </div>
  );
}

export default AboutUs;
