import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import { FaSearch } from "react-icons/fa";
import { Button } from "reactstrap";
import {
  NavItem,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
} from "reactstrap";
import "../scss/header.css";
import AboutUs from "./AboutUs";
import OurServices from "./OurServices";
import Home from "../Layout";
import ContactUs from "./Contact";
import Faqs from "./Faqs";

function Header() {
  return (
    <div className="header">
      <Router>
        <Navbar
          className="d-flex"
          expand="md"
          style={{ boxSizing: "border-box" }}
        >
          <NavbarBrand className="n" href="/" style={{ color: "white"}}>
            MAZZZAYLO PVT.LTD.
          </NavbarBrand>
          <NavbarToggler />
          <div>
            <Collapse navbar>
              <Nav className="mr-auto" navbar>
                <NavItem>
                  <div>
                    <div className="hover-dropdown-main">
                      <Link
                        to="/"
                        className="dropdown-btn"
                        style={{
                          color: "white",
                        }}
                      >
                        HOME
                      </Link>
                    </div>
                  </div>{" "}
                </NavItem>
                &nbsp;
                <NavItem>
                  <div className="hover-dropdown-main">
                    <Link
                      to="/AboutUs"
                      className="dropdown-btn"
                      style={{ color: "white" }}
                    >
                      ABOUT US
                    </Link>
                  </div>
                </NavItem>
                &nbsp;
                <NavItem>
                  <div className="hover-dropdown-main">
                    <Link
                      to="/OurServices"
                      className="dropdown-btn"
                      style={{ color: "white" }}
                    >
                      OUR SERVICES
                    </Link>
                  </div>
                </NavItem>
                &nbsp;
                <NavItem>
                  <div className="hover-dropdown-main">
                    <Link
                      to="/Faqs"
                      className="dropdown-btn"
                      style={{ color: "white" }}
                    >
                      FAQs
                    </Link>
                  </div>
                </NavItem>
                <NavItem>
                  <div className="hover-dropdown-main">
                    <Link
                      to="/Contact"
                      className="dropdown-btn"
                      style={{ color: "white" }}
                    >
                      CONTACT
                    </Link>
                  </div>
                </NavItem>
              </Nav>{" "}
            </Collapse>
          </div>
          <div>
            <Button color="dark" style={{ color: "white", fontSize: 25 }}>
            <FaSearch />
            </Button>{" "}
          </div>
        </Navbar>
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/AboutUs" element={<AboutUs />} />
          <Route exact path="/OurServices" element={<OurServices />} />
          <Route exact path="/COntact" element={<ContactUs />} />
          <Route exact path="/Faqs" element={<Faqs />} />
        </Routes>
      </Router>
    </div>
  );
}

export default Header;
