import React from "react";
import Opportunities from "../Pages/Opportunities";
import Cryptocurrency from "./Cryptocurrency";
import CryptoInfo from "./CryptoInfo";

export default function Home() {
  return (
    <>
      <div style={{ backgroundColor: "black" }}>
        <div style={{ margin: "0px 30px 0px 30px" }}>
          <div style={{ marginTop: "24px" }}>
            <Opportunities />
          </div>
          <div style={{ marginTop: "16px" }}>
            <Cryptocurrency />
          </div>
          <div style={{ margin: "20px 0px 0px 0px" }}>
            <CryptoInfo />
          </div>
        </div>
      </div>
    </>
  );
}
