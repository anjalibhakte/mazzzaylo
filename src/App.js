import "./App.css";
import FooterBoard from "./Components/Layout/Footer";
import Header from "./Components/Pages/Header";

function App() {
  return (
    <>
      <div style={{ backgroundColor: "black"}}>
        <Header />
        <div style={{ marginTop:'20px' }}>
          <FooterBoard />
        </div>
      </div>
    </>
  );
}

export default App;
